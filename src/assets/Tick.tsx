import React from 'react';

const Tick = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    version="1.1"
    viewBox="-0.709 -8.17 141.732 141.732"
    xmlSpace="preserve"
  >
    <path d="M120.238 0L49.753 97.596 11.657 44.848 0 56.506 49.753 125.393 140.314 0z" />
  </svg>
);
export default Tick;
