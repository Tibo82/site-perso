import "./index.scss";
import React from "react";
import IconSmiley from "../../assets/IconSmiley";
import IconGoBack from "../../assets/GoBack";

const NotFoundPage = () => (
  <div>
    <div className="Page404">
      <div className="Text">
        <h2>404 - NotFound</h2>
        <h3>
          Ouuups sorry page not found.
          <ul>
            <li>Enter a valid url path</li>
            <li>Choose a link in the header menu</li>
          </ul>
        </h3>
      </div>
      <IconSmiley />
    </div>
  </div>
);

export default NotFoundPage;
