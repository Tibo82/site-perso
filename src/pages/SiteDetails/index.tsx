import React from "react";

const SiteDetails = () => {
  return (
    <div className="SiteDetails">
      <p>this website is done with Reactjs + TypeScript and :</p>
      <ul>
        <li>Webpack (from scratch)</li>
        <li>Babel</li>
        <li>react-router</li>
        <li>createContext and useReducer for store management</li>
        <li>eslint + prettier </li>
        <li>Bootstrap</li>
        <li>CI/CD via Gitlab</li>
        <li>deploiement via Surge</li>
      </ul>
      <p>
        For more détails, this projet is available on my{" "}
        <a href="https://gitlab.com/Tibo82/site-perso" target="_blank">
          {" "}
          gitlab space
        </a>
      </p>
    </div>
  );
};
export default SiteDetails;
