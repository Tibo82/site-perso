import React, { useContext } from "react";
import { AppContext } from "../../helpers/context";
import { Link } from "react-router-dom";
const Home = () => {
  const { state } = useContext(AppContext);
  const { data } = state;
  const handleCv = () => {
    <Link to={{ pathname: "/cv" }}>{name}</Link>;
  };
  return (
    <div className="Home">
      <div>
        <p>
          {data.introduction.title}
          <br />
          {data.introduction.secondary}
          <br />
          {data.introduction.optional}
        </p>
      </div>
      <div>
        <Link to="/cv">
          <button type="button" className="btn btn-primary" onClick={handleCv}>
            {data.introduction.link}
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Home;
