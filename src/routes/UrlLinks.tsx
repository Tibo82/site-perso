import React from "react";
import { Link } from "react-router-dom";
import { getRoutes } from "./getRoutes";

const UrlLinks = () => {
  const linkComponents = getRoutes.map(({ path, name, id }) =>
    name ? (
      <li key={id}>
        <Link to={{ pathname: path }}>{name}</Link>
      </li>
    ) : null
  );

  return (
    <div>
      <ul>{linkComponents}</ul>
    </div>
  );
};

export default UrlLinks;
