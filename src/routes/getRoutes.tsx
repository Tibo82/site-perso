/* eslint-disable @typescript-eslint/quotes */
import uniqid from "uniqid";
import Home from "../pages/Home";
import Curriculum from "../components/Curriculum";
import NotFoundPage from "../pages/NotFoundPage";
import SiteDetails from "../pages/SiteDetails";

export const getRoutes = [
  {
    path: "/",
    component: Home,
    id: uniqid(),
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
    id: uniqid(),
  },
  {
    path: "/cv",
    name: "Cv",
    component: Curriculum,
    id: uniqid(),
  },
  {
    path: "/about",
    name: "About",
    component: SiteDetails,
    id: uniqid(),
  },
  {
    path: "*",
    component: NotFoundPage,
    id: uniqid(),
  },
];
