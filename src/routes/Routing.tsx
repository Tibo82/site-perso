/* eslint-disable @typescript-eslint/quotes */
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { getRoutes } from "./getRoutes";

const Routing = () => {
  const routeComponents = getRoutes.map(({ path, component, id }) => (
    <Route exact path={path} component={component} key={id} />
  ));
  return <Switch>{routeComponents}</Switch>;
};

export default Routing;
