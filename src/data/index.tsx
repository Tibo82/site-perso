export interface LangInterface {
  header: {
    homepage: string;
    resume: string;
  };
  footer: {
    text: string;
  };
  introduction: {
    title: string;
    secondary: string;
    optional: string;
    link: string;
  };
  downloadButton: {
    text: string;
  };
  presentation: {
    name: string;
    title: string;
  };
  about: {
    title: string;
    content: string;
  };
  social: {
    title: string;
    items: Array<{
      type: string;
      description: string;
      url: string | null;
    }>;
  };
  technologies: {
    title: string;
    items: Array<string>;
  };
  hobbies: {
    title: string;
    items: {
      handball: string;
      running: string;
      travels: string;
      garden: string;
      coding: string;
    };
  };
  experience: {
    title: string;
    items: Array<{
      primary: string;
      secondary: string;
      optional: string;
      description: Array<string>;
      services: Array<{
        name: string;
        url: string;
      }>;
    }>;
  };
  education: {
    title: string;
    items: Array<{
      primary: string;
      secondary: string;
      optional: string;
      description: Array<string>;
    }>;
  };
}
export const fr: LangInterface = {
  header: {
    homepage: "Accueil",
    resume: "CV",
  },
  footer: {
    text: "fait à Toulouse avec React et Webpack from scratch",
  },
  introduction: {
    title: "Je suis Thibault Tripeau.",
    secondary: " Passionné par le developpement.",
    optional:
      "Bienvenue sur ce petit site réalisé avec Reactjs et beaucoup d'Amour",
    link: "Voir mon CV",
  },
  downloadButton: {
    text: "Télécharger le CV",
  },
  presentation: {
    name: "THIBAULT TRIPEAU",
    title: "DEVELOPPEUR & INSTRUMENTISTE",
  },
  about: {
    title: "A PROPOS",
    content:
      "Passionné par les nouvelles technologies et le développement de services web. J'aime le frontend/backend et je m'interesse au DevOps.",
  },
  social: {
    title: "ME CONTACTER",
    items: [
      {
        type: "phone",
        description: "+33.624.96.73.29",
        url: null,
      },
      {
        type: "email",
        description: "thibault.tripeau@yahoo.fr",
        url: null,
      },
      {
        type: "website",
        description: "http://thibault-tripeau.surge.sh",
        url: "http://thibault-tripeau.surge.sh/",
      },
      {
        type: "linkedin",
        description: "thibault-tripeau",
        url: "https://www.linkedin.com/in/thibault-tripeau",
      },
      {
        type: "gitlab",
        description: "tibo82",
        url: "https://gitlab.com/Tibo82",
      },
    ],
  },
  technologies: {
    title: "TECHNOLOGIES",
    items: [
      "Javascript",
      "HTML5/CSS3",
      "React.js",
      "Vue.js",
      "Webpack",
      "Node.js",
      "Express",
      "Linux/MacOs",
      "MongoDB",
      "SqLite",
      "SSR",
      "Gitlab",
      "Github",
      "Jira",
      "Scrum",
    ],
  },
  hobbies: {
    title: "PASSIONS",
    items: {
      handball: "handball",
      garden: "jardinage",
      travels: "voyages",
      running: "courir",
      coding: "Programmation",
    },
  },
  experience: {
    title: "EXPERIENCES",
    items: [
      {
        primary: "Instrumentiste",
        secondary: "à CNS Instrumentation (France)",
        optional: "Fevrier 2021 - Aujourd'hui",
        description: [
          "- Intervention sur les sites industriels pour étalonnage ou mise en service d'instruments de mesures liés a la maitrise de l'eau.",
          "- Développement d'une application pour pilotage d'une sonde de mesure NO3 et NO2.",
          "- Participation au développement commercial sur le secteur Occitanie.",
        ],
        services: [],
      },
      {
        primary: "Développeur Front End",
        secondary: "à SIGFOX (Labège, France)",
        optional: "Mars 2020 - Septembre 2020",
        description: [
          "- Développement d'un plugin sur sketch : intégration d'une interface web utilisateur pour l'implémentations de composants suivant le design system.",
          "- Participation aux sprints dans l'équipe de développeurs, intégration de composants et de fonctionnalités sur l'application 'Bubble'. ",
        ],
        services: [],
      },
      {
        primary: "Géophisicien Terrain Expatrié",
        secondary: "à CGG (Afrique & Europe)",
        optional: "Fevrier 2006 - Aout 2019",
        description: [
          "- Mise en place de scripts Shell, Visual Basic et Python pour faciliter le contrôle du data d'acquisition.",
          "- Control des données, du statut des capteurs et des sources vibratoires.",
          "- Grande expérience sociale et culturelle.",
        ],
        services: [],
      },
    ],
  },
  education: {
    title: "FORMATION",
    items: [
      {
        primary: "Développeur d'Applications Full Stack",
        secondary: "à INP-ENSEEIHT (France)",
        optional: "Octobre 2019 - Septembre 2020",
        description: [
          "- Concevoir, actualiser et modulariser une application sous un system UNIX.",
          "- Environnement Javascript (Nodejs, Reactjs, Vuejs) et Base de données SQL.",
        ],
      },
      {
        primary: "DUT Mesures Physiques",
        secondary: "à UNIVERSITE RABELAIS (Blois, France)",
        optional: " Septembre 1999 -  Juin 2001",
        description: [
          "- Analyser et comprendre rapidement un besoins client sur un sujet scientifique/technique.",
          "- Mettre en place une solution de tests ou de mesures sur une chaîne de production.",
        ],
      },
    ],
  },
};

export const en: LangInterface = {
  header: {
    homepage: "HomePage",
    resume: "Resume",
  },
  footer: {
    text: "done in Montauban with react and TypeScript",
  },
  introduction: {
    title: "I am Thibault Tripeau.",
    secondary: " Passionate about software developement.",
    optional: "Welcome on this website done with Reactjs and so much Love.",
    link: "Check my Resume",
  },
  downloadButton: {
    text: "Télécharger le CV",
  },
  presentation: {
    name: "THIBAULT TRIPEAU",
    title: "DEVELOPER & INSTRUMENTIST",
  },
  about: {
    title: "ABOUT",
    content:
      "Passionate about new technologies and web development. I like frontend/backend and i'm DevOps anthousiast",
  },
  social: {
    title: "I AM SOCIAL",
    items: [
      {
        type: "phone",
        description: "+33.624.96.73.29",
        url: null,
      },
      {
        type: "email",
        description: "thibault.tripeau@yahoo.fr",
        url: null,
      },
      {
        type: "website",
        description: "thibault-tripeau.surge.sh",
        url: "http://thibault-tripeau.surge.sh/",
      },
      {
        type: "linkedin",
        description: "thibault-tripeau",
        url: "https://www.linkedin.com/in/thibault-tripeau",
      },
      {
        type: "gitlab",
        description: "tibo82",
        url: "https://gitlab.com/Tibo82",
      },
    ],
  },
  technologies: {
    title: "TECHNOLOGIES",
    items: [
      "Javascript",
      "HTML5/CSS3",
      "React.js",
      "Vue.js",
      "Webpack",
      "Node.js",
      "Express",
      "Linux/MacOs",
      "MongoDB",
      "SqLite",
      "SSR",
      "Gitlab",
      "Github",
      "Jira",
      "Scrum",
    ],
  },
  hobbies: {
    title: "Hobbies",
    items: {
      handball: "handball",
      garden: "gardening",
      travels: "travels",
      running: "running",
      coding: "coding",
    },
  },
  experience: {
    title: "EXPERIENCES",
    items: [
      {
        primary: "Instrumentist",
        secondary: "@ CNS Instrumentation (France)",
        optional: "February 2021 - Today",
        description: [
          "- Intervention on industrial sites in order to set up or calibrate water control sensors .",
          "- Aplication development for NO3 and NO2 sensor management system .",
          "- Participation of business expension in Occitanie area.",
        ],
        services: [],
      },
      {
        primary: "Front End developer",
        secondary: "@ SIGFOX (Labège, France)",
        optional: "March 2020 - September 2020",
        description: [
          "- Development of a plugin in sketch : user web interface integration to implement components following design system",
          "- Participation of sprints in developer team, components and functionalities implementation into 'Bubble' application. ",
        ],
        services: [],
      },
      {
        primary: "Expatriate Field Geophycist",
        secondary: "@ CGG (Africa & Europe)",
        optional: "February 2006 - August 2019",
        description: [
          "- Coding scripts in Shell , Visual Basic et Python to facilitate acquisition data control.",
          "- Checking of seismic data, sensors status and vibration sources.",
          "- Big social and cultural experience.",
        ],
        services: [],
      },
    ],
  },
  education: {
    title: "EDUCATION",
    items: [
      {
        primary: "Full Stack developer",
        secondary: "@ INP-ENSEEIHT (France)",
        optional: "October 2019 - September 2020",
        description: [
          "- Design, Update and modularize an application under Unix System.",
          "- Javascript environment (Nodejs, Reactjs, Vuejs) and SQL databases.",
        ],
      },
      {
        primary: "DUT Mesures Physiques",
        secondary: "@ RABELAIS UNIVERSITY (Blois, France)",
        optional: " September 1999 -  June 2001",
        description: [
          "- Quickly analyse and understand a custumer request regarding a scientific/technical subject.",
          "- Setup tests or measurements system into a production line.",
        ],
      },
    ],
  },
};
export type dataProps = {
  data: LangInterface;
};
