/* eslint-disable @typescript-eslint/quotes */
import { selectLanguage } from "./selectLanguage";
import { LangInterface } from "../data";

type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? { type: Key }
    : { type: Key; payload: M[Key] };
};

export enum Types {
  ChooseLanguage = "CHOOSE_LANGUAGE",
  SelectedLanguage = "SELECTED_LANGUAGE",
}

type ParameterPayload = {
  [Types.ChooseLanguage]: {
    language: string;
  };
};

export type ParameterActions = ActionMap<ParameterPayload>[keyof ActionMap<
  ParameterPayload
>];

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const parameterReducer = (
  state: LangInterface,
  action: ParameterActions
) => {
  switch (action.type) {
    case Types.ChooseLanguage:
      // eslint-disable-next-line no-case-declarations
      const selectedLanguage = selectLanguage(action.payload.language);
      return (state = selectedLanguage);

    default:
      return state;
  }
};
