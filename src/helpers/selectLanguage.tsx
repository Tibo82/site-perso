import { fr, en } from "../data";

export const selectLanguage = (language: string) => {
  switch (language) {
    case "fr":
      return fr;
    case "en":
      return en;
    default:
      return fr;
  }
};
