import React, { createContext, useReducer, Dispatch } from "react";

import { parameterReducer, ParameterActions } from "./reducers";
import { fr, LangInterface } from "../data";

type InitialStateType = {
  data: LangInterface;
};

export const initialState = {
  data: fr as LangInterface,
  language: "",
};

const AppContext = createContext<{
  state: InitialStateType;
  dispatch: Dispatch<ParameterActions>;
}>({
  state: initialState,
  dispatch: () => null,
});

const mainReducer = ({ data }: InitialStateType, action: ParameterActions) => ({
  data: parameterReducer(data, action as ParameterActions),
});

const AppProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(mainReducer, initialState);
  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};

export { AppContext, AppProvider };
