import React from "react";
import Header from "./components/Header";
import Layout from "./components/Layout";
import Routing from "./routes/Routing";
import { BrowserRouter as Router } from "react-router-dom";
import { AppProvider } from "../src/helpers/context";

const App = () => (
  <div className="AppElments">
    <AppProvider>
      <Router>
        <Header />
        <Layout>
          <Routing />
        </Layout>
      </Router>
    </AppProvider>
  </div>
);
export default App;
