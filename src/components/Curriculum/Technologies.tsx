import React, { useState } from "react";
import { dataProps } from "../../data";

import "../../assets/icons/computer.svg";

const Technologies: React.FunctionComponent<dataProps> = ({
  data,
}): JSX.Element => {
  return (
    <div className="details-categories">
      <div className="detail-title">
        <svg className="svg-white">
          <use href="#computer"></use>
        </svg>
        <p>{data.technologies.title}</p>
      </div>
      <div className="technologies-content">
        {data.technologies.items.map((item: any) => {
          return <div className="item-Technologie">{item}</div>;
        })}
      </div>
    </div>
  );
};

export default Technologies;
