import React, { useContext } from "react";
import { AppContext } from "../../helpers/context";
import "../../assets/icons/graduation.svg";
import "../../assets/icons/business.svg";

import Experience from "./Experience";
import Formation from "./Formation";
import MyDetails from "./MyDetails";

const Curriculum = () => {
  const { state } = useContext(AppContext);

  const { data } = state;
  return (
    <div className="backgroung-cv-page">
      <div className="Cv">
        <div className="Container-left">
          <div className="Container-sm">
            <h1>{data.presentation.name}</h1>
            <h3>{data.presentation.title}</h3>
          </div>
          <MyDetails data={data} />
        </div>
        <div className="Container-right">
          <div className="Container-right-top">
            <h1>{data.presentation.name}</h1>
            <h3>{data.presentation.title}</h3>
          </div>
          <div className="Container-right-bottom">
            <Experience data={data} />
            <Formation data={data} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Curriculum;
