import React, { useState } from "react";
import { dataProps } from "../../data";
import "../../assets/icons/social.svg";
import "../../assets/icons/website.svg";
import "../../assets/icons/phone.svg";
import "../../assets/icons/email.svg";
import "../../assets/icons/linkedin.svg";
import "../../assets/icons/gitlab.svg";

const Social: React.FunctionComponent<dataProps> = ({ data }): JSX.Element => {
  return (
    <div className="details-categories">
      <div className="detail-title">
        <svg className="svg-white">
          <use href="#social"></use>
        </svg>
        <p>{data.social.title}</p>
      </div>
      <div>
        <div className="social-content">
          {data.social.items.map((item: any) => {
            return item.url ? (
              <div>
                <svg className="svg-white-small">
                  <use href={`#${item.type}`}></use>
                </svg>
                <a href={item.url} target="_blank">
                  {item.description}{" "}
                </a>
              </div>
            ) : (
              <div>
                <svg className="svg-white-small">
                  <use href={`#${item.type}`} target="_blank"></use>
                </svg>
                {item.description}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Social;
