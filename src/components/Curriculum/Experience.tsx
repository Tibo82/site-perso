import React, { useState } from "react";
import "../../assets/icons/business.svg";
import { dataProps } from "../../data";

const Experience: React.FunctionComponent<dataProps> = ({
  data,
}): JSX.Element => {
  return (
    <div className="categories">
      <div className="title-categories">
        <svg className="svg-black">
          <use href="#business"></use>
        </svg>
        <p>{data.experience.title}</p>
      </div>
      {data.experience.items.map((categorie: any) => {
        return (
          <div>
            <div className="heading">
              <span className="heading-primary">{categorie.primary}</span>
              <span className="heading-secondary"> {categorie.secondary}</span>

              <div className="heading-optional">
                <p>{categorie.optional}</p>
              </div>
              {categorie.description.map((task: any) => {
                return (
                  <div className="heading-description">
                    <p className="description-interline">{task}</p>
                  </div>
                );
              })}

              <div className="heading-services">
                {categorie.services.length > 0 && (
                  <p className="title-services">services: </p>
                )}
                {categorie.services.length > 0 &&
                  categorie.services.map((service: any) => {
                    return <a href={service.url}>{service.name} </a>;
                  })}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Experience;
