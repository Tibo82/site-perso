import React, { useState } from "react";
import { dataProps } from "../../data";

import "../../assets/icons/profile.svg";

const About: React.FunctionComponent<dataProps> = ({ data }): JSX.Element => {
  return (
    <div className="details-categories">
      <div className="detail-title">
        <svg className="svg-white">
          <use href="#profile"></use>
        </svg>
        <p>{data.about.title}</p>
      </div>

      <div className="about-content">
        <p>{data.about.content}</p>
      </div>
    </div>
  );
};

export default About;
