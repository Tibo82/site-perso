import React from "react";
import { dataProps } from "../../data";
import "../../assets/icons/handball.svg";
import "../../assets/icons/garden.svg";
import "../../assets/icons/travels.svg";
import "../../assets/icons/running.svg";
import "../../assets/icons/coding.svg";
import "../../assets/icons/happy.svg";
const Passions: React.FunctionComponent<dataProps> = ({
  data,
}): JSX.Element => {
  return (
    <div className="details-categories">
      <div className="detail-title">
        <svg className="svg-white">
          <use href="#happy"></use>
        </svg>
        <p>{data.hobbies.title}</p>
      </div>
      <div className="technologies-content">
        {Object.entries(data.hobbies.items).map((item: any) => {
          return (
            <div className="hobbies-content">
              <svg className="svg-white">
                <use href={`#${item[0]}`}></use>
              </svg>
              <div>{item[1]}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Passions;
