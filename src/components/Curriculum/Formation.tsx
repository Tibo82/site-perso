import React, { useState } from "react";
import "../../assets/icons/graduation.svg";
import { dataProps } from "../../data";

const Formation: React.FunctionComponent<dataProps> = ({
  data,
}): JSX.Element => {
  return (
    <div className="categories">
      <div className="title-categories">
        <svg className="svg-black">
          <use href="#graduation"></use>
        </svg>
        <p> {data.education.title}</p>
      </div>
      {data.education.items.map((categorie: any) => {
        return (
          <div>
            <div className="heading">
              <span className="heading-primary">{categorie.primary}</span>
              <span className="heading-secondary"> {categorie.secondary}</span>

              <div className="heading-optional">
                <p>{categorie.optional}</p>
              </div>
              {categorie.description.map((task: any) => {
                return (
                  <div className="heading-description">
                    <p className="description-interline">{task}</p>
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Formation;
