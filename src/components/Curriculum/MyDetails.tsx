import React from "react";
import About from "./About";
import Social from "./Social";
import Technologies from "./Technologies";
import Passions from "./Passions";
import { dataProps } from "../../data";

const MyDetails: React.FunctionComponent<dataProps> = ({
  data,
}): JSX.Element => {
  return (
    <div className="details">
      <div className="position-image">
        <div className="cvImage"></div>
      </div>
      <About data={data} />
      <Social data={data} />
      <Technologies data={data} />
      <Passions data={data} />
    </div>
  );
};

export default MyDetails;
