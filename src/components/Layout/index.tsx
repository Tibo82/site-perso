import React from "react";
type Props = {
  children: React.ReactElement;
};

const Layout = ({ children }: Props): JSX.Element => (
  <div className="Layout">{children}</div>
);

export default Layout;
