import React, { useContext } from "react";
import { AppContext } from "../../helpers/context";
import { Types } from "../../helpers/reducers";

const SelectLang = () => {
  const { dispatch } = useContext(AppContext);
  const handleChange = (event: any) => {
    dispatch({
      type: Types.ChooseLanguage,
      payload: { language: event.target.value },
    });
  };
  return (
    <div className="Select">
      <select name="langue" onChange={handleChange} className="custom-select">
        <option value="fr">FR</option>
        <option value="en">EN</option>
      </select>
    </div>
  );
};
export default SelectLang;
