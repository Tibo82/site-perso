import React from "react";
import Menu from "./Menu";
import SelectLang from "../SelectLang";
const Header = () => (
  <div className="header">
    <div className="roundedImage"></div>
    <div>
      <header>
        <Menu />
        <SelectLang />
      </header>
    </div>
  </div>
);

export default Header;
