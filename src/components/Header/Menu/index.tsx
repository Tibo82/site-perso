import React from "react";
import UrlLinks from "../../../routes/UrlLinks";

const Menu = () => {
  return (
    <div>
      <UrlLinks />
    </div>
  );
};

export default Menu;
